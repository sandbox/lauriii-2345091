<?php
/**
 * @file
 * Contains \Drupal\demo\Form\DemoForm.
 */
namespace Drupal\form_test\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface; 

class FormTest extends FormBase {
   
  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'form_test';
  }
   
  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
     
    $form['checkbox'] = array(
      '#type' => 'checkbox',
      '#title' => t('Checkbox'),
    );
    $form['checkboxes'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Checkboxes'),
      '#options' => array(t('Option 1'), t('Option 2')),
    );
    $form['container'] = array(
      '#type' => 'container',
    );
    $form['container']['textfield'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Textfield inside container'),
    );
    $form['date'] = array(
      '#type' => 'date',
      '#title' => t('Date'),
    );
    $form['details'] = array(
      '#type' => 'details',
      '#title' => t('Details'),
    );
    $form['fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Fieldset'),
    );

    $form['fieldset'] = array(
      '#type' => 'fieldset',
      '#required' => TRUE,
      '#title' => t('Fieldset required'),
    );
    $form['file'] = array(
      '#type' => 'file',
      '#title' => t('File'),
    );
    $form['hidden'] = array(
      '#type' => 'hidden',
    );
    $form['item'] = array(
      '#type' => 'item',
      '#title' => t('Item'),
      '#markup' => t('Testing'),
    );
    $form['machine_name'] = array(
      '#type' => 'machine_name',
    );
    $form['managed_file'] = array(
      '#type' => 'managed_file',
      '#title' => t('Managed file'),
    );
    $form['markup'] = array(
      '#markup' => t('Markup'),
    );
    $form['password'] = array(
      '#type' => 'password',
      '#title' => t('Password'),
    );
    $form['password'] = array(
      '#type' => 'password_confirm',
      '#title' => t('Password form elements'),
    );
    $form['radio'] = array(
      '#type' => 'radio',
      '#title' => t('Radio'),
    );
    $form['radios'] = array(
      '#type' => 'radios',
      '#title' => t('Radios'),
      '#options' => array(t('Option 1'), t('Option 2')),
    );
    $form['select'] = array(
      '#type' => 'select',
      '#title' => t('select'),
      '#options' => array(t('Option 1'), t('Option 2')),
    );
    $form['description'] = array(
      '#type' => 'text_format',
      '#title' => t('Text format'),
    );
    $form['textarea'] = array(
      '#type' => 'textarea',
      '#title' => t('Textarea'),
    );
    $form['textfield'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Textfield'),
    );
    $form['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight'),
    );
    $form['email'] = array(
      '#type' => 'email',
      '#title' => $this->t('Email'),
    );
    $form['tel'] = array(
      '#type' => 'tel',
      '#title' => $this->t('Tel')
    );
    $form['show'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    );
     
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }
}
